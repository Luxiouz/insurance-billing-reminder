import logo from './logo.svg';
import './App.css';
import Header from './components/layout/Header';
import Products from './components/layout/Products';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import ProductForm from './components/layout/Products/ProductForm';

function App() {
  return (
    <main>
      <Header />
      <section className='container py-3'>
        <BrowserRouter>
          <Routes>
            <Route path='/' element={<Products />} />
            <Route path='/products' element={<Products />} />
            <Route path='/product/create' element={<ProductForm />} />
            <Route path='/product/:operation/:id' element={<ProductForm />} />
            <Route path='*' element={<h1 className='text-center'>404 NOT FOUND!</h1>} />
          </Routes>
        </BrowserRouter>
      </section>

    </main>

  );
}

export default App;
