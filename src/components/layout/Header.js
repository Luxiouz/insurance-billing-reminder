import React from 'react'

export default function Header() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light" >
  <div className="container-fluid">
    <a className="navbar-brand" href="#">Insurance Billing Reminder</a>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li className="nav-item">
          <a className="nav-link active" aria-current="page" href="#"><i className="bi bi-people-fill d-inline-block pe-1"></i>Customers</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#"><i className="bi bi-file-earmark-medical-fill d-inline-block pe-1"></i>Products</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#"><i className="bi bi-piggy-bank-fill d-inline-block pe-1"></i>Discounts</a>
        </li>
        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Dropdown
          </a>
          <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a className="dropdown-item" href="#">Action</a></li>
            <li><a className="dropdown-item" href="#">Another action</a></li>
            <li><hr className="dropdown-divider" /></li>
            <li><a className="dropdown-item" href="#">Something else here</a></li>
          </ul>
        </li>
      </ul>
      <div className="d-flex justify-content-end align-items-center">
        <button className="btn btn-outline-primary ms-2" type="submit"><i className="bi bi-person-circle d-inline-block pe-1"></i>Login</button>
      </div>
    </div>
  </div>
</nav>
  )
}
