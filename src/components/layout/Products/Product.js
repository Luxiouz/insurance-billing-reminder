import React from 'react'
import { Link } from 'react-router-dom'
import Currency from '../../../utils/currency';

export default function Product({ product }) {

    const getCurrencySymbol = () => {
        console.log("checking currency")
        switch (product.currency) {
            case Currency.USD:
                return 'USD $';
            case Currency.PEN:
                return 'S/. ';
            case Currency.ARS:
                return 'ARS $';
            default:
                return '--'
        }
    }

    return (
        <li key={product.id} className="list-group-item p-3">
            <div className="product-container d-flex justify-content-between align-items-start">
                <div className="ms-2 me-auto">
                    <div className="fw-bold"><i className="bi bi-file-earmark-check pe-1"></i>{product.name}</div>
                    {product.description}
                </div>
                <div className='d-flex flex-column me-4'>
                    <span className="badge bg-primary rounded-pill">{
                        getCurrencySymbol()
                    }{product.price}</span>
                    <small>each {product.frequency} days</small>
                </div>
                <Link to={`/product/edit/${product.id}`} className='product__container--btn-edit ms-4 p-0'><i className="bi bi-pencil-square text-muted"></i></Link>
            </div>
        </li>
    )
}
