import React, { useCallback, useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom';
import { db } from '../../../config/firebase';
import FormMode from '../../../utils/formMode';
import { errorAlert, showAlert, successAlert } from '../../../utils/sharedAlerts';
import Loader from '../shared/Loader';
import './index.css';

const initialState = {
    name: '',
    description: '',
    price: '',
    currency: '',
    frequency: '',
}

export default function ProductForm() {

    const [product, setProduct] = useState(initialState);
    const [formMode, setFormMode] = useState(FormMode.CREATE);
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    const { operation, id } = useParams();

    const getProduct = useCallback(async () => {
        setLoading(true);
        try {
            const productData = await db.collection('products').doc(id).get();
            const data = productData.data();
            const productFormat = {
                id: productData.id,
                ...data,
            };

            return productFormat;
        } catch (error) {
            errorAlert(error.message);
        } finally {
            setLoading(false);
        }

    }, [id]);

    useEffect(() => {
        setFormMode(operation?operation:'create');
        const firebaseConfig = {
            apiKey: process.env.REACT_APP_APIKEY,
            authDomain: process.env.REACT_APP_AUTHDOMAIN,
            projectId: process.env.REACT_APP_PROJECTID,
            storageBucket: process.env.REACT_APP_STORAGEBUCKET,
            messagingSenderId: process.env.REACT_APP_MESSAGINGSENDERID,
            appId: process.env.REACT_APP_APPID,
        };
        console.log("config", firebaseConfig)
    }, [operation])

    useEffect(() => {
        const gettingProduct = async () => {
            if (formMode === FormMode.EDIT && id) {
                const productFromDB = await getProduct();
                console.log(productFromDB)
                setProduct(productFromDB);
            }
        }
        gettingProduct();
    }, [formMode, getProduct, id])


    const createProduct = async () => {
        setLoading(true);
        try {
            await db.collection('products').add(product);
            successAlert('Product Created', `Product ${product.name} was created successfully.`, () => {
                navigate('/');
            }, false);
        } catch (error) {
            errorAlert(error?.message);
        } finally {
            setLoading(false);
        }
    }


    const handleChange = (e) => {
        setProduct({ ...product, [e.target.name]: e.target.value });
    }

    const updateProduct = async () => {
        setLoading(true);
        try {
            const productUpdate = { ...product };
            delete productUpdate.id;
            await db.collection('products').doc(product.id).update(productUpdate);
            successAlert('Product Updated', `Product ${product.name} was updated successfully.`, () => {
                navigate('/');
            }, false);
        } catch (error) {
            errorAlert(error?.message);
        } finally {
            setLoading(false);
        }
    }

    const deleteProduct = async () => {
        setLoading(true);
        try {
            showAlert('warning', 'Delete Product', `Product ${product.name} will be removed. Do you want to continue?`, async () => {
                await db.collection('products').doc(id).delete();
                successAlert('Product removed', `Product ${product.name} was removed successfully.`, () => {
                    navigate('/');
                }, false);
            });

        } catch (error) {
            errorAlert(error?.message);
        } finally {
            setLoading(false);
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (formMode === FormMode.CREATE) createProduct();
        else updateProduct();
    }

    return (
        <>
            {loading && <Loader />}
            <h2>{formMode === FormMode.CREATE ? 'Create Insurance Product' : 'Edit Insurance Product'}</h2>
            <form className='form-inline' onSubmit={handleSubmit}>
                <div className="form-group mb-3">
                    <label htmlFor="product-name" className="form-label">Name:</label>
                    <input type="text" className="form-control" name='name' id="product-name" value={product.name} onChange={handleChange} required />
                </div>
                <div className="form-group mb-3">
                    <label htmlFor="product-description" className="form-label">Description:</label>
                    <textarea style={{ resize: 'none' }} type="text" className="form-control" rows={2} name='description' id="product-description" value={product.description} onChange={handleChange} maxLength="250" required />
                </div>


                <div className="form-group mb-3">
                    <div className='row me-0 pe-0'>
                        <div className='col-md-6'>
                            <label htmlFor="product-price" className="form-label">Price:</label>
                            <div className='row'>
                                <div className='col-3 pe-0'>
                                    <select className="form-control product__price__currency pe-0" name='currency' id="product-currency" value={product.currency} onChange={handleChange} required >
                                        <option value='' key="NONE">--</option>
                                        <option value="USD" key="USD">USD $</option>
                                        <option value="PEN" key="PEN">PEN S/.</option>
                                        <option value="ARS" key="ARS">ARS $</option>
                                    </select>
                                </div>
                                <div className='col-9 ps-0'>
                                    <input type="number" className="form-control product__price__amount col-8 ps-2" name='price' id="product-price" value={product.price} onChange={handleChange} required />
                                </div>
                            </div>
                        </div>
                        <div className='col-md-6 pe-0'>
                            <label htmlFor="product-frequency" className="form-label">Payment Frequency (days):</label>
                            <input type="number" className="form-control" name='frequency' id="product-frequency" value={product.frequency} onChange={handleChange} required />
                        </div>
                    </div>
                </div>

                <button type="submit" className="btn btn-primary" disabled={loading}>{formMode === FormMode.CREATE ? 'SAVE' : 'UPDATE'}</button>
                {formMode === FormMode.EDIT && <button type="button" className="btn btn-danger ms-2" disabled={loading} onClick={deleteProduct}>DELETE</button>}
            </form>
        </>
    )
}

/*
 name: 'Soat Particular',
        description: 'Seguro obligatorio para accidentes de tránsito para vehiculos particulares',
        price: 75,
        currency: 'PEN',
        frequency: 360,*/