import React from 'react'
import Product from './Product'

export default function ProductList({ products }) {
  return (
    <ul className="list-group">
      {products.map(product =>
        <Product key={product.id} product={product}/>
      )}
    </ul>
  )
}
