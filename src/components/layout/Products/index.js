import React, { useEffect, useState } from 'react'
import ProductList from './ProductList'
import './index.css';
import { db } from '../../../config/firebase';
import { errorAlert } from '../../../utils/sharedAlerts';
import Loading from '../shared/Loader'
import { Link } from 'react-router-dom';
import classNames from 'classnames'

export default function Products() {

    const [products, setProducts] = useState([])
    const [loading, setLoading] = useState(false);
    const [sortAZ, setSortAZ] = useState(false);

    useEffect(() => {
        getProducts();
    }, [])

    const getProducts = async (dispatch) => {
        setLoading(true);
        try {
            const productsDB = [];
            const info = await db.collection('products').get();
            info.forEach((item) => {
                productsDB.push({
                    id: item.id,
                    ...item.data(),
                });
            });
            setProducts(productsDB);
        } catch (error) {
            errorAlert(error.message);
        } finally {
            setLoading(false);
        }
    };

    const handleSort = () => {
        setSortAZ(!sortAZ);
        const sortedProducts = products.sort((a, b) => !sortAZ ? a.name.localeCompare(b.name) : b.name.localeCompare(a.name));
        setProducts(sortedProducts);
    }


    return (
        <>
            {loading && <Loading />}

            {loading ? <p>Loading products...</p> :
                <div>
                    <div className="d-flex justify-content-between align-items-center">
                        <div className="d-flex justify-content-start align-items-center">
                            <h2>Products</h2>
                            <Link to='/product/create' className='btn btn-success ms-3 p-1' style={{ height: 'fit-content' }}><i className="bi bi-file-earmark-plus"></i>Create</Link>
                        </div>
                        <button className='btn btn-outline' onClick={handleSort}>
                            <i className={classNames({
                                'bi btn-sort__icon': true,
                                'bi-sort-alpha-down': !sortAZ,
                                'bi-sort-alpha-up': sortAZ,
                            })}></i>
                            </button>
                    </div>
                    <section className='products__container'>
                        {products.length > 0 ? <ProductList products={products} /> : <p>There are not products created yet.</p>}
                    </section>
                </div>
            }
        </>

    )
}
