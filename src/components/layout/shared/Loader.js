import React from 'react'

export default function Loader() {
    return (
        <div className='d-flex justify-content-center align-items-center vw-100 vh-100 z-index-master position-absolute top-0 start-0' style={{ backgroundColor: 'rgba(146, 201, 249, 0.8)' }}>
            <div className="spinner-grow text-primary" role="status">
                <span className="visually-hidden">Loading...</span>
            </div>
        </div>
    )
}
