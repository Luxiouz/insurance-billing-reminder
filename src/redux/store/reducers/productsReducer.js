const initialState = {
    loading: false,
    products: [],
    product: {},
    created: false,
    updated: false,
    deleted: false,
}

export const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'CREATE_PRODUCT':
            return {
                ...state,
                created: action.payload,
            }
        case 'GET_PRODUCTS':
            return {
                ...state,
                products: action.payload,
            }
        case 'GET_PRODUCT':
            return {
                ...state,
                product: action.payload,
            }
        case 'UPDATE_PRODUCT':
            return {
                ...state,
                updated: action.payload,
            }
        case 'DELETE_PRODUCT':
            return {
                ...state,
                deleted: action.payload,
            }
        case 'CLEAN_STATE':
            return {
                ...state,
                created: false,
                updated: false,
                deleted: false,
            }
        default:
            return state;
    }
}
