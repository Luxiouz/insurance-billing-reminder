export const discounts = [
    {
        id: 'd1',
        name: 'Soat Naboo',
        value: 5,
        type: 'percent',
    },
    {
        id: 'd2',
        name: 'EPS Corouscant 002',
        value: 50,
        type: 'amount',
    },
    {
        id: 'd3',
        name: 'Health Fit 006',
        value: 7.5,
        type: 'percent',
    },
];

export const products = [
    {
        id: 'p1',
        name: 'Soat Particular',
        description: 'Seguro obligatorio para accidentes de tránsito para vehiculos particulares',
        price: 75,
        currency: 'PEN',
        frequency: 360,
    },
    {
        id: 'p2',
        name: 'Soat Urbano',
        description: 'Seguro obligatorio para accidentes de tránsito para vehiculos de transporte urbano',
        price: 300,
        currency: 'PEN',
        frequency: 360,
    },
    {
        id: 'p3',
        name: 'Seguro de salud Light',
        description: 'Seguro de salud para atención ambulatoria en clínicas afiliadas del sector I',
        price: 175,
        currency: 'PEN',
        frequency: 30,
    },
    {
        id: 'p4',
        name: 'Seguro de salud Medium',
        description: 'Seguro de salud para atención ambulatoria en clínicas afiliadas del sector I y II',
        price: 245,
        currency: 'PEN',
        frequency: 30,
    },
    {
        id: 'p5',
        name: 'Seguro de vida Básico',
        description: 'Seguro de vida ante invalidez y fallecimiento',
        price: 65,
        currency: 'PEN',
        frequency: 30,
    },
    {
        id: 'p6',
        name: 'Seguro de cambio brusco de dolar',
        description: 'Seguro ante pérdida de cambios bruscos del dólar.',
        price: 10,
        currency: 'USD',
        frequency: 30,
    },
]