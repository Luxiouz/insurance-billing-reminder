import Swal from "sweetalert2"

export const errorAlert = (message, allowOutsideClick = true) => {
    Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: message || 'Something went wrong!',
        allowOutsideClick: allowOutsideClick,
      })
}

export const successAlert = (title, message, onConfirm, allowOutsideClick = true) => {
    Swal.fire({
        icon: 'success',
        title: title || 'Success',
        text: message || 'Successful Operation!',
        allowOutsideClick: allowOutsideClick,
      }).then((result) => {
          result.isConfirmed && onConfirm && onConfirm();
      });
}

export const showAlert = (icon, title, message, onConfirm, showCancelBtn=false, allowOutsideClick = true) => {
    Swal.fire({
        icon: icon,
        title: title || 'Success',
        text: message || 'Successful Operation!',
        cancelButtonText: 'cancel',
        showCancelButton: showCancelBtn,
        allowOutsideClick: allowOutsideClick,
      }).then((result) => {
        result.isConfirmed && onConfirm && onConfirm();
      });
}